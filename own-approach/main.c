#include <stdint.h>
#include <inttypes.h>
#include <stdbool.h>

#define GPIO(bank) ((struct gpio* ) ((0x40058000) + (0x1000 * (bank))))
#define RCGC ((struct rcgc*) (0x400fe000 + 0x600)) //Note that we skip directly to RCGCWD

#define PIN(bank, num) ((((bank) - 'A') << 8) | (num))
#define PINNO(pin) (pin & 255)
#define PINBANK(pin) (pin >> 8)
#define BIT(x) (1UL << (x))

//We'll just add a byte array to align it 
//Offsets are bytes
struct gpio {
    volatile uint32_t DATA;
    volatile uint8_t reserved_1[0x400]; //We just take up this much space
    volatile uint32_t DIR, ISe, IBE, IEV, IM, IS, MIS, RIS, IC, AFSEL;
    volatile uint8_t reserved_2[0x80]; //We just take up this much space
    volatile uint32_t R2R, R4R, R8R, DR, ODR, PUR, PDR, SLR, DEN, LOCK, CR, AMSEL, PCTL, ADCCTL, DMACTL;
};

struct rcgc { //TODO Add UART and stuff later
    volatile uint32_t WD, TIMER, GPIO;
};

enum {GPIO_MODE_INPUT, GPIO_MODE_OUTPUT, GPIO_MODE_AFSEL, GPIO_MODE_ANALOG};

static inline void gpio_set_mode(uint16_t pin, uint8_t mode) {
    struct gpio * gpio = GPIO(PINBANK(pin));
    int n = PINNO(pin);
    gpio->DATA &= ~(3U << (n * 2));
    gpio->DATA |= (mode & 3) << (n * 2);
};

static inline void gpio_write(uint16_t pin, bool val) {
    struct gpio *gpio = GPIO(PINNO(pin));
    if (val) {
        gpio->PUR = 1;
    }
    else {
        gpio->PDR = 1;
    }
};

#define SYSCTL_RCGCGPIO_R (*(( volatile unsigned long *)0x400FE608 ) )
#define GPIO_PORTF_DATA_R (*(( volatile unsigned long *)0x40025038 ) )
#define GPIO_PORTF_DIR_R (*(( volatile unsigned long *)0x40025400 ) )
#define GPIO_PORTF_DEN_R (*(( volatile unsigned long *)0x4002551C ) )
#define GPIO_PORTF_CLK_EN 0x20
#define GPIO_PORTF_PIN1_EN 0x02
#define LED_ON1 0x02
#define DELAY_VALUE 3333333
static volatile unsigned long j=0;
void Delay(void);
void Delay(void){
    for (j=0; j<DELAY_VALUE ; j++);
}

void gpio_data_write(struct gpio* gpio, int mask, int val) {
    struct gpio *masked = (unsigned long*) gpio + mask;
    masked |= val;
}

int main ( void )
{
    RCGC->GPIO |= BIT(5);
    struct gpio *gpio = (volatile unsigned long *)0x40025000; //GPIO(PINBANK(led));
    //gpio->DIR |= BIT(1);
    //gpio->DEN |= BIT(1);
    GPIO_PORTF_DIR_R |= BIT(1) ;
    GPIO_PORTF_DEN_R |= BIT(1) ;
    // Loop forever .
    while (1)
    {
        //GPIO_PORTF_DATA_R |= BIT(1);
        gpio->DATA |= BIT(1);
        //gpio_data_write(gpio, 38, BIT(1));

        Delay();
    }
    for (;;) (void) 0;
    return 0;
}


__attribute__((naked, noreturn)) void _reset(void) {
    // memset .bss to zero, and copy .data section to RAM region
    extern long _sbss, _ebss, _sdata, _edata, _sidata;
    for (long *dst = &_sbss; dst < &_ebss; dst++) *dst = 0;
    for (long *dst = &_sdata, *src = &_sidata; dst < &_edata;) *dst++ = *src++;

    main();             // Call main()
    for (;;) (void) 0;  // Infinite loop in the case if main() returns
};


extern void _estack(void);

__attribute__((section(".vectors"))) void (*const tab[16+91])(void) = {
    _estack, _reset
};

