#define SYSCTL_RCGCGPIO_R (*(( volatile unsigned long *)0x400FE608 ) )
#define GPIO_PORTF_DATA_R (*(( volatile unsigned long *)0x40025038 ) )
#define GPIO_PORTF_DIR_R (*(( volatile unsigned long *)0x40025400 ) )
#define GPIO_PORTF_DEN_R (*(( volatile unsigned long *)0x4002551C ) )
#define GPIO_PORTF_CLK_EN 0x20
#define GPIO_PORTF_PIN1_EN 0x02
#define LED_ON1 0x02
#define GPIO_PORTF_PIN2_EN 0x04
#define LED_ON2 0x04
#define GPIO_PORTF_PIN3_EN 0x08
#define LED_ON3 0x08
#define DELAY_VALUE 3333333
static volatile unsigned long j=0;
void Delay(void);
void Delay(void){
    for (j=0; j<DELAY_VALUE ; j++);
}
int main ( void )
{
    SYSCTL_RCGCGPIO_R |= GPIO_PORTF_CLK_EN ;
    GPIO_PORTF_DIR_R |= GPIO_PORTF_PIN1_EN ;
    GPIO_PORTF_DEN_R |= GPIO_PORTF_PIN1_EN ;
    GPIO_PORTF_DIR_R |= GPIO_PORTF_PIN2_EN ;
    GPIO_PORTF_DEN_R |= GPIO_PORTF_PIN2_EN ;
    GPIO_PORTF_DIR_R |= GPIO_PORTF_PIN3_EN ;
    GPIO_PORTF_DEN_R |= GPIO_PORTF_PIN3_EN ;
    // Loop forever .
    while (1)
    {
        GPIO_PORTF_DATA_R &= LED_ON3;
        GPIO_PORTF_DATA_R &= LED_ON2;
        GPIO_PORTF_DATA_R |= LED_ON1;
        Delay();
        GPIO_PORTF_DATA_R &= LED_ON1;
        GPIO_PORTF_DATA_R &= LED_ON2;
        GPIO_PORTF_DATA_R |= LED_ON3;
        Delay();
        GPIO_PORTF_DATA_R &= LED_ON3;
        GPIO_PORTF_DATA_R &= LED_ON1;
        GPIO_PORTF_DATA_R |= LED_ON2;
        Delay();
    }
}

__attribute__((naked, noreturn)) void _reset(void) {
  // memset .bss to zero, and copy .data section to RAM region
  extern long _sbss, _ebss, _sdata, _edata, _sidata;
  for (long *dst = &_sbss; dst < &_ebss; dst++) *dst = 0;
  for (long *dst = &_sdata, *src = &_sidata; dst < &_edata;) *dst++ = *src++;

  main();             // Call main()
  for (;;) (void) 0;  // Infinite loop in the case if main() returns
}

extern void _estack(void);  // Defined in link.ld

// 16 standard and 91 STM32-specific handlers
__attribute__((section(".vectors"))) void (*const tab[16 + 91])(void) = {
    _estack, _reset};
