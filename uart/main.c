#include <stdint.h>
#include <stdio.h>
#include <string.h>
#define BIT(x) (1UL << (x))

//Should redefine base + offset seperately for nicer arithmatic
#define RCGCUART (*((volatile unsigned long*) (0x400fe618)))
#define RCGCGPIO (*((volatile unsigned long*) (0x400fe608)))

#define GPIOA_AFSEL (*((volatile unsigned long*) (0x40004420)))
#define GPIOA_PCTL (*((volatile unsigned long*) (0x4000452c)))
#define GPIOA_DEN (*((volatile unsigned long*) (0x4000451c)))

#define UART0_CTL (*((volatile unsigned long*) (0x4000C030)))
#define UART0_IBRD (*((volatile unsigned long*) (0x4000C024)))
#define UART0_FBRD (*((volatile unsigned long*) (0x4000C028)))
#define UART0_LCRH (*((volatile unsigned long*) (0x4000C02c)))
#define UART0_CC (*((volatile unsigned long*) (0x4000Cfc8)))
#define UART0_FR (*((volatile unsigned long*) (0x4000C018)))
#define UART0_DR (*((volatile unsigned long*) (0x4000C000)))


#define GPIOA(a) (*((volatile unsigned long*) (0x40004000 + a)))
#define GPIOB(a) (*((volatile unsigned long*) (0x40005000 + a)))
#define GPIOC(a) (*((volatile unsigned long*) (0x40006000 + a)))
#define GPIOD(a) (*((volatile unsigned long*) (0x40007000 + a)))
#define GPIOE(a) (*((volatile unsigned long*) (0x40024000 + a)))
#define GPIOF(a) (*((volatile unsigned long*) (0x40025000 + a)))

#define AFSEL (0x420)
#define AMSEL (0x528)
#define PCTL (0x52c)
#define DEN (0x51c)
#define DIR (0x400)
#define LOCK (0x520)
#define PUR (0x510)
#define CR (0x524)
#define DATA(a) (a)

void printChar(char c) {
    //Wait for the FIFO to empty
    while ((UART0_FR & BIT(5)) != 0);
    UART0_DR = c; //Then write value
};


void printStr(char* string, int len) {
    int count = 0;
    while (count < len) {
        printChar(string[count]);
        count++;
    }
};

//syscalls to implement printf
int _fstat(int fd, struct stat * st) { (void) fd, (void) st; return -1; }

//This is used for malloc
void *_sbrk(int incr) { 
    extern char _end;
    static unsigned char *heap = NULL;
    unsigned char *prev_heap;
    if (heap == NULL) heap = (unsigned char*) &_end;
    prev_heap = heap;
    heap+= incr;
    return prev_heap;
}

int _close(int fd) { (void) fd; return -1; }

//Anything and everything is tty
int _isatty(int fd) { (void) fd; return 1; }

int _read(int fd, char *ptr, int len) { (void) fd, (void) ptr, (void) len; return -1; }

int _lseek(int fd, int ptr, int dir) { (void) fd, (void) ptr, (void) dir; return -1; }

int _write(int fd, char* ptr, int len) {
    (void) fd, (void) ptr, (void) len;
    if (fd == 1) printStr(ptr, len);
    return 0;
}


//Delay macro taken from lab
#define DELAY_VALUE 333333
static volatile unsigned long j=0;
void Delay(unsigned int delay);
void Delay(unsigned int delay) {
    volatile unsigned int i, counter;
    counter = delay * 4000;
    for (j=0; j<counter ; j++);
}

void uart_init() {
    //Initialize the UART
    RCGCUART |= BIT(0);
    RCGCGPIO |= BIT(0);

    RCGCUART |= BIT(0);

    GPIOA_AFSEL |= BIT(1) | BIT(0);
    GPIOA_PCTL |= BIT(0) | BIT(4);
    GPIOA_DEN |= BIT(0) | BIT(1);

    UART0_CTL &= ~(1<<0);
    //This is baud rate stuff
    UART0_IBRD = 104;
    UART0_FBRD = 11;

    UART0_LCRH = (0X3 << 5);
    UART0_CC = 0x0;

    UART0_CTL = BIT(0) | BIT(8) | BIT(9);
    printf("\r\n");
};

void portb_init() {
    GPIOB(DEN) = 0xFF;
    GPIOB(DIR) = 0xFF;
    GPIOB(PCTL) &= ~(0xFF);
    GPIOB(AMSEL) &= ~(0xFF);
    GPIOB(AFSEL) &= ~(0xFF);
};



void portf_init(void)
{
    GPIOF(LOCK) = 0x4C4F434B;
    GPIOF(CR) = 0x11;
    GPIOF(PUR) |= 0x11;
    GPIOF(LOCK) = 0x00;
    GPIOF(DEN) = 0xff;
    GPIOF(DIR) = 0x00;
};


static unsigned int seed = 1;
void srand (int newseed) {
    seed = (unsigned)newseed & 0x7fffffffU;
}

int rand (void) {
    seed = (seed * 1103515245U + 12345U) & 0x7fffffffU;
    return (int)seed;
}

int min (a, b) { 
    return ((a > b) ? b : a);
}

int main ( void )
{
    uart_init(); //convenience function I've used to initialize the UART connection 
    RCGCGPIO |= BIT(1) | BIT(5); 

    Delay(10);

    portb_init();
    portf_init();

    unsigned char digitPattern[] = {0xc0, 0xf9, 0xa4, 0xb0, 0x99, 0x92, 0x82, 0xf8, 0x80, 0x90};

    unsigned char units[] = {0,8,3,9,4};
    volatile unsigned int i = 0;
    volatile unsigned int flag = 0;
    srand(19);
    volatile unsigned int state = 1;
    volatile unsigned int random_number = 1;
    volatile unsigned int random_second = 1;

    while (1) {
        if (GPIOF(DATA(0x3fc))) {
            printf("%x : %x : %x : %x \r\n", GPIOF(DATA(0x3fc)), flag, digitPattern[units[i]], units[i]);
            Delay(160);
            if ((!flag) & !(GPIOF(DATA(0x3fc)) & 0x10)) { i++; flag = 1;} //make so can't happen at the same time
            else if ((!flag) & !(GPIOF(DATA(0x3fc)) & 0x01)) { i--; flag = 1;}
            else {flag = 0;}
            i = i % 5;
        };
        GPIOB(DATA(0x3fc)) = digitPattern[units[i]];
    }



    for (;;) (void) 0;
    return 0;
}


//won't return, and we don't want the assembler to make prologue and epilogue for us
__attribute__((naked, noreturn)) void _reset(void) {
    // memset .bss to zero, and copy .data section to RAM region
    extern long _sbss, _ebss, _sdata, _edata, _sidata;
    for (long *dst = &_sbss; dst < &_ebss; dst++) *dst = 0;
    for (long *dst = &_sdata, *src = &_sidata; dst < &_edata;) *dst++ = *src++;

    main();             // Call main()
    for (;;) (void) 0;  // Infinite loop in the case if main() returns
};


extern void _estack(void);

__attribute__((section(".vectors"))) void (*const tab[16+91])(void) = {
    _estack, _reset
};

